## Get Started

1. From the root, run `npm install`.
2. Change in package.json if you are developing in html or wp like this ` "development": "html",`
3. Run `gulp` to build and watch for changes.
4. The main html files are rendered if you save with `_.html` at the end
5. for include a html partials just use `@@include ('path/to/file.html')`
